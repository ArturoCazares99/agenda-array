package com.example.agenda;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    final ArrayList<Contacto> contactos = new ArrayList<Contacto>();
    private EditText edtNombre;
    private EditText edtTelefono;
    private EditText edtTelefono2;
    private EditText edtDireccion;
    private EditText edtNotas;
    private CheckBox cbxFavorito;
    private Contacto saveContact;
    private int saveIndex;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtNombre = findViewById(R.id.txtNombre);
        edtTelefono = findViewById(R.id.txtTel1);
        edtTelefono2 = findViewById(R.id.txtTel2);
        edtDireccion = findViewById(R.id.txtDomicilio);
        edtNotas = findViewById(R.id.txtNota);
        cbxFavorito = findViewById(R.id.chkFavorito);

        Button btnGuardar = findViewById(R.id.btnGuardar);
        Button btnLimpiar = findViewById(R.id.btnLimpiar);
        Button btnListar = findViewById(R.id.btnListar);
        Button btnSalir = findViewById(R.id.btnSalir);

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edtNombre.getText().toString().equals("") || edtTelefono.getText().toString().equals("") || edtDireccion.getText().toString().equals("")){
                    Toast.makeText(MainActivity.this, R.string.msgError, Toast.LENGTH_SHORT).show();
                }else{
                    Contacto nContacto = new Contacto();
                    int index = contactos.size();
                    if(saveContact != null){
                        contactos.remove(saveIndex);
                        nContacto=saveContact;
                        index=saveIndex;
                    }
                    nContacto.setNombre(edtNombre.getText().toString());
                    nContacto.setDomicilio(edtDireccion.getText().toString());
                    nContacto.setTelefono1(edtTelefono.getText().toString());
                    nContacto.setTelefono2(edtTelefono2.getText().toString());
                    nContacto.setNotas(edtNotas.getText().toString());
                    nContacto.setFavorito(cbxFavorito.isChecked());
                    contactos.add(index, nContacto);
                    Toast.makeText(MainActivity.this, R.string.mensaje, Toast.LENGTH_SHORT).show();
                    limpiar();
                }
            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });
        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(MainActivity.this, ListActivity.class);
                Bundle bObject = new Bundle();
                bObject.putSerializable("contactos", contactos);
                i.putExtras(bObject);
                startActivityForResult(i,0);
            }
        });
        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void limpiar(){
        saveContact=null;
        edtNombre.setText("");
        edtTelefono.setText("");
        edtTelefono2.setText("");
        edtDireccion.setText("");
        edtNotas.setText("");
        cbxFavorito.setChecked(false);
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent intent){
        super.onActivityResult(requestCode,resultCode,intent);
        if(intent != null){
            Bundle oBundle = intent.getExtras();
            if(oBundle.getBoolean("modify") == true){
                saveContact = (Contacto) oBundle.getSerializable("contacto");
                saveIndex = oBundle.getInt("index");
                edtNombre.setText(saveContact.getNombre());
                edtTelefono.setText(saveContact.getTelefono1());
                edtTelefono2.setText(saveContact.getTelefono2());
                edtDireccion.setText(saveContact.getDomicilio());
                edtNotas.setText(saveContact.getNotas());
                cbxFavorito.setChecked(saveContact.isFavorito());
            }
            else{
                int id = oBundle.getInt("id");
                Toast.makeText(MainActivity.this, contactos.get(id).getNombre()+" ha sido eliminado.", Toast.LENGTH_SHORT).show();
                contactos.remove(id);
                limpiar();
            }
        }else{
            limpiar();
        }
    }
}
