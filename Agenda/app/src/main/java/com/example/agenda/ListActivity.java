package com.example.agenda;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.SearchView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class ListActivity extends AppCompatActivity {
    private TableLayout tblLista;
    private ArrayList<Contacto> contactos;
    private ArrayList<Contacto> bufferList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        tblLista = findViewById(R.id.tblLista);

        Bundle bundleObject = getIntent().getExtras();
        contactos = (ArrayList<Contacto>) bundleObject.getSerializable("contactos");
        bufferList = contactos;
        Button btnNuevo = findViewById(R.id.btnNuevo);

        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        });
        cargarContactos();
    }
    public void cargarContactos(){
        for(int x=0; x<contactos.size();x++){
            final Contacto c = new Contacto(contactos.get(x));
            TableRow nRow = new TableRow(ListActivity.this);

            TextView nText = new TextView(ListActivity.this);
            nText.setText(c.getNombre());

            nText.setTextSize(TypedValue.COMPLEX_UNIT_PT,6);
            nText.setTextColor((c.isFavorito())? Color.BLUE: Color.BLACK);
            nRow.addView(nText);

            Button nButton = new Button(ListActivity.this);
            nButton.setText(R.string.accver);
            nButton.setTextSize(TypedValue.COMPLEX_UNIT_PT,6);
            nButton.setTextColor(Color.BLACK);
            nButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for (int y=0;y<bufferList.size();y++){
                        if(c.getNombre().matches(bufferList.get(y).getNombre()) && c.getTelefono1().matches(bufferList.get(y).getTelefono1())) {
                            Contacto c = (Contacto) v.getTag(R.string.contacto_g);
                            Intent i = new Intent();
                            Bundle oBundle = new Bundle();
                            oBundle.putSerializable("contacto", c);
                            oBundle.putInt("index",y);
                            oBundle.putBoolean("modify", true);
                            i.putExtras(oBundle);
                            setResult(RESULT_OK, i);
                            finish();
                        }
                    }
                }
            });
            nButton.setTag(R.string.contacto_g,c);
            nButton.setTag(R.string.contacto_g_index,x);
            Button dButton = new Button(ListActivity.this);
            dButton.setText("Borrar");
            dButton.setTextSize(TypedValue.COMPLEX_UNIT_PT,6);
            dButton.setTextColor(Color.RED);
            dButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent();
                    Bundle oBundle = new Bundle();
                    for(int y=0; y<bufferList.size();y++){
                        if(bufferList.get(y).getNombre().matches(c.getNombre()) && bufferList.get(y).getTelefono1().matches(c.getTelefono1()) && bufferList.get(y).getTelefono2().matches(c.getTelefono2()) && bufferList.get(y).getNotas().matches(c.getNotas()) && bufferList.get(y).getDomicilio().matches(c.getDomicilio())) {
                            oBundle.putInt("id",y);
                        }
                    }
                    oBundle.putBoolean("modify", false);
                    i.putExtras(oBundle);
                    setResult(RESULT_OK, i);
                    finish();
                }
            });
            nRow.addView(nButton);
            nRow.addView(dButton);

            tblLista.addView(nRow);
        }
    }

    private void searchOnTable(String queryText){
        ArrayList<Contacto> filterList = new ArrayList<>();
        for (int i=0;i<bufferList.size();i++){
            String name = bufferList.get(i).getNombre().toLowerCase();
            if(name.contains(queryText.toLowerCase())){
                filterList.add(bufferList.get(i));
            }
        }
        contactos = filterList;
        tblLista.removeAllViews();
        TableRow headRow = new TableRow(ListActivity.this);
        TextView nombre = new TextView(ListActivity.this);
        nombre.setText(R.string.nombre);
        nombre.setTextSize(TypedValue.COMPLEX_UNIT_PT,8);
        TextView accion = new TextView(ListActivity.this);
        accion.setText(R.string.accion);
        accion.setTextSize(TypedValue.COMPLEX_UNIT_PT,8);
        headRow.addView(nombre);
        headRow.addView(accion);
        tblLista.addView(headRow);
        cargarContactos();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.searchview,menu);
        MenuItem menuItem = menu.findItem(R.id.menu_search);
        SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                searchOnTable(newText);
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }
}
